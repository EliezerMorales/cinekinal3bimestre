package gerber.org.login;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by oscar on 19/05/2015.
 */
public class ManejadorUsuario {
    List<Usuario> Usuarios = new ArrayList<Usuario>();
    private static ManejadorUsuario instancia;


    public void agregarUsuario(String nombreUsuario,String pass){
        Usuarios.add(new Usuario(nombreUsuario,pass));
    }

    public List<Usuario> obtenerUsuarios(){
        return this.Usuarios;
    }
    public static ManejadorUsuario getInstancia(){
        if(instancia==null)
            instancia=new ManejadorUsuario();
        return instancia;
    }



}

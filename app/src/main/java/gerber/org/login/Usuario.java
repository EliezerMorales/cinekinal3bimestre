package gerber.org.login;

/**
 * Created by oscar on 19/05/2015.
 */
public class Usuario {
    private String nombreUsuario;

    private String pass;

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }


    public Usuario(String nombreUsuario ,String pass){
            this.nombreUsuario = nombreUsuario;

            this.pass = pass;
        }

}
